import 'dart:math';
import 'dart:ui';

import 'package:animation_tutorial/Classes/bar.dart';
import 'package:animation_tutorial/Classes/barChart.dart';
import 'package:animation_tutorial/painter.dart/barChartPainter.dart';
import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(home: ChartPage()));

class ChartPage extends StatefulWidget {
  @override
  _ChartPageState createState() => _ChartPageState();
}

class _ChartPageState extends State<ChartPage> with TickerProviderStateMixin {
  final Random random = Random();
  AnimationController animation;
  BarChartTween tween;
  static const size = const Size(200.0, 100.0);

  void changeData() {
    setState(() {
      tween =
          BarChartTween(tween.evaluate(animation), BarChart.random(size, random),);
      animation.forward(from: 0.0);
    });
  }

  @override
  void initState() {
    super.initState();
    animation = AnimationController(
      duration: Duration(milliseconds: 300),
      vsync: this,
    );
    tween = BarChartTween(BarChart.empty(size), BarChart.random(size, random));
    animation.forward();
  }

  @override
  void dispose() {
    super.dispose();
    animation.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: CustomPaint(
          size: size,
          painter: BarChartPainter(tween.animate(animation)),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.refresh),
        onPressed: changeData,
      ),
    );
  }
}
