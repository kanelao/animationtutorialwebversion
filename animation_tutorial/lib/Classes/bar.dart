import 'dart:math';
import 'dart:ui';

import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';
import 'color_palette.dart';

class Bar {
  final double height;
  final Color color;
  final double x;
  final double width;

  Bar(
      {@required this.height,
      @required this.color,
      @required this.width,
      @required this.x});

  Bar get collapsed => Bar(x: x, height: 0.0, width: 0.0, color: color);

  static Bar lerp(Bar begin, Bar end, double t) {
    return Bar(
      height: lerpDouble(begin.height, end.height, t),
      color: Color.lerp(begin.color, end.color, t),
      x: lerpDouble(begin.x, end.x, t),
      width: lerpDouble(begin.width, end.width, t),
    );
  }
}

class BarTween extends Tween<Bar> {
  BarTween(Bar begin, Bar end) : super(begin: begin, end: end);

  @override
  Bar lerp(double t) => Bar.lerp(begin, end, t);
}
