import 'package:animation_tutorial/Classes/barChart.dart';
import 'package:flutter/material.dart';

class BarChartPainter extends CustomPainter {
  final Animation<BarChart> animation;

  BarChartPainter(Animation<BarChart> animation)
      : animation = animation,
        super(repaint: animation);

  @override
  void paint(Canvas canvas, Size size) {
    final Paint paint = Paint()..style = PaintingStyle.fill;
    final chart = animation.value;
    for (final bar in chart.bars) {
      paint.color = bar.color;
      Rect barRect =
          Rect.fromLTWH(bar.x, size.height - bar.height, bar.width, bar.height);
      canvas.drawRect(barRect, paint);
    }
  }

  @override
  bool shouldRepaint(BarChartPainter oldDelegate) => false;
}
